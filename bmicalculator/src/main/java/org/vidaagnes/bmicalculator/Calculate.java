package org.vidaagnes.bmicalculator;

public class Calculate {

	private double weight;
	private double height;
	private double bmi;
	
	
	
	
	public double getBmi() {
		return bmi;
	}

	public void setBmi(double bmi) {
		this.bmi = bmi;
	}

	public double getWeight() {
		return weight;
	}

	public double getHeight() {
		return height;
	}
	
	public Calculate (double w, double h, double b) {
		this.weight=w;
		this.height=h;		
		this.bmi=b;
	}
	
	
	public double szamol (double w, double h) {
		this.weight=w;
		this.height=h;
		this.bmi=w/(h*h);
		return bmi;
	}

	

}
