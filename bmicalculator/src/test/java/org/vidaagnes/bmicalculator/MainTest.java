package org.vidaagnes.bmicalculator;

import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.Before;


public class MainTest {

	private Main tester;
	@Before
	public void setUp() throws Exception{
		tester =new Main();
	}
	
	@Test
	public void test() {
		Main test=new Main();
		final double DELTA = 1e-15;
		double result=test.szamol(52, 1.56);
		assertEquals(21.367521367521366, result, DELTA);
		
	}

}
